service iptables restart
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -Z
iptables -t nat -Z
iptables -A INPUT -s 111.204.113.130/32 -j ACCEPT 
iptables -A INPUT -s 106.39.52.244/32 -j ACCEPT 
iptables -A INPUT -s 192.168.11.0/24 -j ACCEPT 
iptables -A INPUT -s 192.168.12.0/24 -j ACCEPT 
iptables -A INPUT -s 118.145.8.96/29 -j ACCEPT 
iptables -A INPUT -s 117.79.230.16/28 -j ACCEPT 
iptables -A INPUT -s 117.79.229.72/29 -j ACCEPT 
iptables -A INPUT -s 210.14.132.224/29 -j ACCEPT 
iptables -A INPUT -d 127.0.0.0/8 -j ACCEPT 
iptables -A INPUT -d 224.0.0.0/8 -j ACCEPT
iptables -A INPUT -p vrrp -j ACCEPT
iptables -A INPUT -p udp -m udp --sport 53 -j ACCEPT 
iptables -A INPUT -p tcp -m tcp --sport 53 -j ACCEPT 
iptables -A INPUT -p tcp -m state --state RELATED,ESTABLISHED -j ACCEPT 
iptables -A INPUT -p tcp -m multiport --dport 80,8080,21,20,2337,3005,3015,1935 -j ACCEPT
iptables -A INPUT -p tcp -m multiport --dport 3021:3023 -j ACCEPT
iptables -A INPUT -p tcp -m multiport --dport 3049:3056 -j ACCEPT
iptables -A INPUT -p tcp -m multiport --dport 4049:4056 -j ACCEPT
iptables -A INPUT -p tcp -m multiport --dport 6051:6059 -j ACCEPT
iptables -A INPUT -p icmp -j ACCEPT 
iptables -P INPUT DROP
iptables -A OUTPUT -d 118.145.8.96/29 -j ACCEPT 
iptables -A OUTPUT -d 117.79.230.16/28 -j ACCEPT 
iptables -A OUTPUT -d 117.79.229.72/29 -j ACCEPT 
iptables -A OUTPUT -d 210.14.132.224/29 -j ACCEPT 
iptables -A OUTPUT -d 192.168.11.0/24 -j ACCEPT 
iptables -A OUTPUT -d 192.168.12.0/24 -j ACCEPT 
iptables -A OUTPUT -d 111.204.113.130/32 -j ACCEPT 
iptables -A OUTPUT -d 106.39.52.244/32 -j ACCEPT 
iptables -A OUTPUT -p udp -m udp --dport 53 -j ACCEPT 
iptables -A OUTPUT -p tcp -m tcp --dport 53 -j ACCEPT 
iptables -A OUTPUT -p tcp -m multiport --dport 80,8080,1935,8883 -j ACCEPT
iptables -A OUTPUT -p tcp -m state --state NEW -j REJECT --reject-with icmp-port-unreachable 
iptables -A OUTPUT -p udp -j REJECT --reject-with icmp-port-unreachable 
service iptables save
chkconfig iptables on
