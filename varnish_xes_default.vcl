import std;

probe healthcheck {
  .url = "/check_nginx";
  .interval = 60s;
  .timeout = 2s;
  .window = 3;
  .threshold = 3;
  .initial = 3;
  .expected_response = 200;
}

backend allweb1 {
  .host = "210.14.132.230";
  .port = "80";
  .connect_timeout = 8s;
  .first_byte_timeout = 20s;
  .between_bytes_timeout = 5s;
  .probe = healthcheck;
}

backend allweb2 {
  .host = "210.14.132.231";
  .port = "80";
  .connect_timeout = 8s;
  .first_byte_timeout = 20s;
  .between_bytes_timeout = 5s;
  .probe = healthcheck;
}

backend tikuweb1 {
  .host = "210.14.132.238";
  .port = "80";
  .connect_timeout = 8s;
  .first_byte_timeout = 20s;
  .between_bytes_timeout = 5s;
  .probe = healthcheck;
}

backend tikuweb2 {
  .host = "210.14.132.239";
  .port = "80";
  .connect_timeout = 8s;
  .first_byte_timeout = 20s;
  .between_bytes_timeout = 5s;
  .probe = {
	.url="/check_nginx";
	.interval = 60s;
	.timeout = 2s;
	.window = 3;
	.threshold = 3;
	.initial = 3;
	.expected_response = 200;
	}
}

director allweb random {
    {
       .backend = allweb1;
       .weight = 1;
    }
    {
       .backend = allweb2;
       .weight = 1;
    }
}

director tikuweb fallback {
    {
       .backend = tikuweb1;
    }
    {
       .backend = tikuweb2;
    }
}

acl purgeAllow {
     "localhost";
     "192.168.13.99";
}

sub vcl_recv {
   if (req.request == "PURGE") {
        #判断是否允许ip
        if (!client.ip ~ purgeAllow) {
             error 405 "Not allowed.";
        }
        #去缓存中查找
        return (lookup);
    }

    std.log("LOG_DEBUG: URL=" + req.url);

    if(req.http.host !~ "admin.q.haibian.com$")  {  
		set req.backend = allweb;  
    } else { 
		set req.backend = tikuweb;  
    }  

    if (req.request != "GET" && req.request != "HEAD" && req.request != "PUT" && req.request != "POST" && req.request != "TRACE" && req.request != "OPTIONS" && req.request != "DELETE") {
         return (pipe);
    }
    # 只缓存 GET 和 HEAD 请求
    if (req.request != "GET" && req.request != "HEAD") {
        std.log("LOG_DEBUG: req.request not get!  " + req.request );
        return(pass);
    }

#    if (req.http.Authorization || req.http.Cookie) {
    if (req.http.Authorization) {
         return (pass);
    }

   if (req.url ~ "\.(jpeg|jpg|png|gif|bmp|tif|tiff|ico|wmf|js|css|ejs|swf|txt|zip|exe|html|htm)(\?.*|)$") {
        std.log("*** url is jpeg|jpg|png|gif|ico|js|css|txt|zip|exe|html|htm set cached! ***");
	#下面是忽略静态文件后面的 ?
#	set req.url = regsub(req.url, "\?.*$", "");
        unset req.http.cookie;
        if (req.http.Accept-Encoding) {
            if (req.url ~ "\.(jpg|png|gif|jpeg)(\?.*|)$") {
                remove req.http.Accept-Encoding;
            } elsif (req.http.Accept-Encoding ~ "gzip") {
                set req.http.Accept-Encoding = "gzip";
            } elsif (req.http.Accept-Encoding ~ "deflate") {
                set req.http.Accept-Encoding = "deflate";
            } else {
                remove req.http.Accept-Encoding;
            }
        }
        return(lookup);
    } else {
        std.log("url is not cached!");
        return (pass);
    }
}


sub vcl_hit {
    if (req.request == "PURGE") {
        set obj.ttl = 0s;
        error 200 "Purged.";
    }
    return (deliver);
}

sub vcl_miss {
    std.log("################# cache miss ################### url=" + req.url);
    if (req.request == "PURGE") {
        purge;
        error 200 "Purged.";
    }
}

sub vcl_fetch {
    if (beresp.status == 500 || beresp.status == 501 || beresp.status == 502 || beresp.status == 503 || beresp.status == 504) {
        std.log("beresp.status error!!! beresp.status=" + beresp.status);
        set req.http.host = "status";
        set beresp.saintmode = 20s;
        return (restart);
    }
    if (beresp.http.Pragma ~ "no-cache" || beresp.http.Cache-Control ~ "no-cache" || beresp.http.Cache-Control ~ "private") {
        std.log("not allow cached!   beresp.http.Cache-Control=" + beresp.http.Cache-Control);
	return (hit_for_pass);
    }
    if (beresp.ttl <= 0s || beresp.http.Set-Cookie || beresp.http.Vary == "*") {
        set beresp.ttl = 120 s;
        return (hit_for_pass);
    }

    if (req.request == "GET" && req.url ~ "\.(css|js|ejs|html|htm)$") {
        std.log("gzip is enable.");
        set beresp.do_gzip = true;
        set beresp.ttl = 2d;
    }

    if (req.request == "GET" && req.url ~ "\.(jpeg|jpg|png|gif|bmp|tif|tiff|ico|wmf|js|css|ejs|swf|txt|zip|exe)(\?.*|)$") {
        std.log("url css|js|gif|jpg|jpeg|bmp|png|tiff|tif|ico|swf|exe|zip|bmp|wmf is cache 2h!");
        set beresp.ttl = 2d;
    } elseif (req.request == "GET" && req.url ~ "\.(html|htm)$") {
        set beresp.ttl = 2d;
    } else {
        return (hit_for_pass);
    }
 
    if (!req.backend.healthy) {
        std.log("eq.backend not healthy! req.grace = 1m");
        set req.grace = 1m;
    } else {
        set req.grace = 30s;
    }
     return (deliver);
}

sub vcl_deliver {
    set resp.http.x-hits = obj.hits;
    remove resp.http.Server;
    remove resp.http.Via;
    set resp.http.Server = "varnish_99";
    if ( obj.hits > 0 ) {
	set resp.http.X-Cache = "hits from changkuan_web99";
    } else {
	set resp.http.X-Cache = "miss from changkuan_web99";
    }
    return (deliver);
}
